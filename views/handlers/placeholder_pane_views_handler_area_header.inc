<?php
/**
 * @file
 * Renders a placeholder in views.
 */

class placeholder_pane_views_handler_area_header extends views_handler_area {
  public function option_definition() {
    $options = parent::option_definition();
    $options['placeholder'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $placeholders = placeholder_pane_get_placeholders();
    $placeholder_options = array();
    foreach ($placeholders as $key => $placeholder) {
      $placeholder_options[$key] = $placeholder['placeholder_title'];
    }

    $form['placeholder'] = array(
      '#type' => 'select',
      '#title' => t('Placeholder'),
      '#options' => $placeholder_options,
      '#description' => t('Choose the placeholder you want to display in the area.'),
      '#default_value' => $this->options['placeholder'],
    );

    return $form;
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
  }
  
  public function render($empty = FALSE) {
    if (!$empty) {
      return $this->render_placeholder($this->options['placeholder']);
    }
    return '';
  }

  public function render_placeholder($placeholder) {
    $tenant = placeholder_pane_tenant_load($placeholder);
    if ($tenant) {
      $placeholder = placeholder_pane_tenant_view($tenant);
      return render($placeholder);
    }
    else {
      return '';
    }
  }

}