<?php

/**
 * @file
 * Ctools content_type plugin
 *
 * Provides a pane that serves as placeholder
 * which can be occupied by a tenant later.
 *
 * When the pane is created, one selects
 * the bundles that allowed to occupy it.
 */

$plugin = array(
  'title' => t('Placeholder pane'),
  'no title override' => TRUE,
  'category' => array(t('Placeholder panes')),
  'edit form' => '',
  'all contexts' => TRUE,
  'render callback' => 'placeholder_pane_render',
  'admin title' => 'placeholder_pane_admin_title',
  'defaults' => array('title' => '', 'placeholder' => '', 'allowed_bundles' => array(), 'module' => 'placeholder_pane'),
);

/**
 * Return all content types available.
 */
function placeholder_pane_placeholder_pane_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__);
  if (isset($types)) {
    return $types;
  }
  $types = array();

  // default type for adding new placeholder panes
  $types['placeholder_pane'] = array(
    'name' => 'placeholder_pane',
    'title' => t('New placeholder pane'),
    'top level' => TRUE,
    'category' => t('Placeholder panes'),
    'description' => t('Create a new placeholder pane'),
    'edit form' => 'placeholder_pane_edit_form',
    'all contexts' => TRUE,
  );
  // get already existent placeholders
  foreach (placeholder_pane_get_placeholders() as $name => $placeholder) {
    $types[$name] = _placeholder_pane_content_type_wrap($placeholder);
  }

  return $types;
}

function _placeholder_pane_content_type_wrap($placeholder) {
  $info = array(
    'name' => $placeholder['placeholder'],
    'title' => check_plain($placeholder['placeholder_title']),
    'category' => t('Placeholder panes'),
    'all contexts' => TRUE,
    // Store this here to make it easy to access.
    'placeholder' => $placeholder,
    'edit form' => 'placeholder_pane_edit_form',
    'check editable' => '_placeholder_type_content_type_editable',
  );

  return $info;
}

function _placeholder_type_content_type_editable($content_type, $subtype, $conf) {
  if (!empty($subtype['placeholder']['module']) && $subtype['placeholder']['module'] === 'placeholder_pane') {
    return TRUE;
  }
  return FALSE;
}

/**
 * Return the custom content types with the specified $subtype_id.
 */
function placeholder_pane_placeholder_pane_content_type_content_type($subtype_id) {
  $types = placeholder_pane_placeholder_pane_content_type_content_types();
  return $types[$subtype_id];
}

/**
 * Returns the administrative title for a placeholder.
 */
function placeholder_pane_admin_title($subtype, $conf) {

  $placeholder = placeholder_pane_load($subtype);
  if (!empty($placeholder->tenant_id)) {
    $tenant = placeholder_pane_tenant_load($placeholder);
    if (!empty($tenant->title)) {
      return $placeholder->placeholder_title . ' / '. $tenant->title;
    }
    else {
      return $placeholder->placeholder_title;
    }
  }
  return $subtype;
}

function placeholder_pane_render($subtype, $conf, $args, $context) {
  $placeholder = placeholder_pane_load($subtype);

  if (empty($placeholder->tenant_id)) {
    return;
  }

  global $language;
  $tenant = placeholder_pane_tenant_load($placeholder, $language->language);

  $block = new stdClass();
  $block->module = $placeholder->tenant_entity_type;
  $block->delta = $tenant->placeholder->tenant_id;

  $block->content = placeholder_pane_tenant_view($tenant, $language->language);

  return $block;
}


function placeholder_pane_edit_form($form, &$form_state) {
  $conf = array();

  if (!empty($form_state['pane']->type) && !empty($form_state['pane']->subtype) && $form_state['pane']->type === 'placeholder_pane') {
    $placeholder = placeholder_pane_load($form_state['pane']->subtype);
    $conf = array(
      'title' => $placeholder->placeholder_title,
      'placeholder' => $placeholder->placeholder,
      'allowed_bundles' => $placeholder->placeholder_settings['allowed_bundles'],
      'module' => $placeholder->module,
    );
  }

  if (empty($conf['module'])) {
    $conf['module'] = 'placeholder_pane';
  }

  // node bundles
  $node_types = node_type_get_types();
  $options = array();
  foreach ($node_types as $type => $detail) {
    $key = 'node:' . $type;
    if ($conf['module'] === 'placeholder_pane' || in_array($key, $conf['allowed_bundles'])) {
      $options[$key] = $detail->name;
    }
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative title'),
    '#default_value' => empty($conf['title']) ? array() : $conf['title'],
    '#required' => TRUE,
    '#disabled' => $conf['module'] !== 'placeholder_pane',
  );

  $form['placeholder'] = array(
    '#type' => 'machine_name',
    '#default_value' => empty($conf['placeholder']) ? array() : $conf['placeholder'],
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => 'placeholder_pane_exists',
      'source' => array('title'),
    ),
    '#required' => TRUE,
    '#disabled' => !empty($conf['placeholder']),
  );


  $form['allowed_bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed bundles'),
    '#options' => $options,
    '#default_value' => empty($conf['allowed_bundles']) ? array() : $conf['allowed_bundles'],
    '#required' => TRUE,
    '#disabled' => $conf['module'] !== 'placeholder_pane',
  );

  $form['module'] = array(
    '#type' => 'value',
    '#value' => empty($conf['module']) ? 'placeholder_pane' : $conf['module'],
  );

  return $form;
}

/**
 * Submit function, note anything in the form_state[conf]
 * automatically gets saved to pane settings
 */
function placeholder_pane_edit_form_submit(&$form, &$form_state) {
  $form_state['values']['allowed_bundles'] = array_filter($form_state['values']['allowed_bundles']);
  $form_state['pane']->subtype = $form_state['values']['placeholder'];

  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = isset($form_state['values'][$key]) ? $form_state['values'][$key] : $form_state['plugin']['defaults'][$key];
  }

}

function placeholder_pane_exists($name) {
  $types = placeholder_pane_placeholder_pane_content_type_content_types();

  return !empty($types[$name]);
}
