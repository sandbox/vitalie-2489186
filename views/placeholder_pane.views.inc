<?php

/**
 * @file
 * Provide views data for placeholders.
 */

/**
 * Implements hook_views_data().
 */
function placeholder_pane_views_data() {
  $data = array();

  $data['placeholders']['table']['group'] = t('Placeholders');
  $data['placeholders']['table']['join'] = array(
    // #global let's it appear all the time.
    '#global' => array(),
  );
  $data['placeholders']['placeholder_header'] = array(
    'title' => t('Placeholder header'),
    'help' => t('Displays a placeholder area.'),
    'area' => array(
      'handler' => 'placeholder_pane_views_handler_area_header',
    ),
  );

  return $data;
}