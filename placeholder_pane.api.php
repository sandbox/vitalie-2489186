<?php

/**
 * Return an array of placeholder objects
 * keyed by unique machine name
 *
 * If you define a machine name that already exists,
 * you placeholder might not be active,
 * depending on the order of module_implements results
 */
function hook_placeholder_pane_info() {
  $placeholders = array(
    // there is a limit of 64 chars for a placeholder name
    'placeholder_name_1' => array(
      'placeholder_title' => 'My placeholder 1',
      'placeholder_settings' => array(
        // bundles instances of which can occupy the placeholder
        'allowed_bundles' => array('node:page', 'node:article'),
        // make this placeholder available as drupal block
        'as_block' => TRUE,
      )
    )
  );

  return $placeholders;
}