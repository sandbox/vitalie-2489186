<?php
/**
 * @file
 * Placeholder pane module file.
 *
 * A higher level administrator can define placeholder panes
 * which will then be occupied by a lower lever administrator.
 *
 * At this moment only nodes can be placed into placeholder panes.
 *
 * A motivation for this module is the fact that it allows panels to be exported
 * as configuration without dependencies on content.
 */

/**
 * Implements hook_permission
 * @return array
 */
function placeholder_pane_permission() {
  return array(
    'occupy placeholders' => array(
      'title' => t('Occupy placeholders'),
      'description' => t('Allows users to place entities into existing placeholders'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function placeholder_pane_block_info() {
  $blocks = array();

  // expose placeholders that want to be exposed as blocks
  $placeholders = placeholder_pane_get_placeholders();
  foreach ($placeholders as $placeholder) {
    if (!empty($placeholder['placeholder_settings']['as_block'])) {
      $blocks[$placeholder['placeholder']] = array(
        'info' => $placeholder['placeholder_title'],
      );
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function placeholder_pane_block_view($delta = '') {
  $block = array();

  $tenant = placeholder_pane_tenant_load($delta);
  if ($tenant) {
    $block['subject'] = $tenant->title;
    $block['content'] = placeholder_pane_tenant_view($tenant);
  }

  return $block;
}

/**
 * Implements hook_placeholder_pane_info().
 */
function placeholder_pane_placeholder_pane_info() {
  $placeholders = array();

  $result = db_query('SELECT placeholder, module, placeholder_title, placeholder_settings FROM {placeholder_panes} ORDER BY placeholder_title')->fetchAllAssoc('placeholder', PDO::FETCH_ASSOC);
  foreach ($result as $name => $placeholder) {
    if (!empty($placeholder['placeholder_settings'])) {
      $placeholder['placeholder_settings'] = unserialize($placeholder['placeholder_settings']);
    }
    else {
      $placeholder['placeholder_settings'] = array();
    }
    $placeholders[$name] = $placeholder;
  }
  return $placeholders;
}

/*
 * Get all placeholders
 */
function placeholder_pane_get_placeholders() {
  $placeholders = &drupal_static(__FUNCTION__);

  if (is_array($placeholders)) {
    return $placeholders;
  }

  // those in db have priority
  $placeholders = placeholder_pane_placeholder_pane_info();

  // loop through other modules to collect definitions of placeholders
  $no_need = cache_get('placeholder_pane:complete');
  if (!empty($no_need->data)) {
    return $placeholders;
  }

  foreach (module_implements('placeholder_pane_info') as $module) {
    if ($module == 'placeholder_pane') {
      continue;
    }
    $module_placeholders = module_invoke($module, 'placeholder_pane_info');
    foreach ($module_placeholders as $name => $placeholder) {
      if (empty($placeholders[$name])) {
        // place placeholder name inside placeholder for convenience
        $placeholder['placeholder'] = $name;
        $placeholder['module'] = $module;
        if (placeholder_pane_register($placeholder)) {
          $placeholders[$name] = $placeholder;
        }
      }
    }
  }
  cache_set('placeholder_pane:complete', 1);
  return $placeholders;
}

/**
 * Saves placeholder definition to the db
 * Returns true if success
 *
 * @param $placeholder
 *
 */
function placeholder_pane_register($placeholder) {

  if (strlen($placeholder['placeholder']) > 64) {
    drupal_set_message(t('Placeholder name should be at most 64 characters long. The @placeholder placeholder will not function normally.', array('@placeholder' => $placeholder['placeholder'])), 'error');
    return FALSE;
  }

  $fields = array();
  foreach (array('placeholder', 'module', 'placeholder_title', 'placeholder_settings') as $key) {
    if (empty($placeholder[$key])) {
      return FALSE;
    }
    $fields[$key] = $placeholder[$key];
  }
  $fields['placeholder_settings'] = serialize($fields['placeholder_settings']);

  try {
    db_merge('placeholder_panes')
      ->key(array('placeholder' => $fields['placeholder']))
      ->fields($fields)
      ->execute();
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

function placeholder_pane_get_view_modes($type) {
  $bundle = field_extract_bundle('node', $type);
  $entity_info = entity_get_info('node');
  $view_modes = field_view_mode_settings('node', $bundle);
  $options = array('' => t('Full content'));
  if (!empty($entity_info['view modes'])) {
    foreach ($entity_info['view modes'] as $mode => $settings) {
      if (!empty($view_modes[$mode]['custom_settings'])) {
        $options[$mode] = $settings['label'];
      }
    }
  }
  return $options;
}

function placeholder_pane_node_delete($node) {
  db_update('placeholder_panes')
    ->fields(array(
      'tenant_entity_type' => NULL,
      'tenant_id' => NULL,
      'tenant_mode' => NULL,
    ))
    ->condition('tenant_entity_type', 'node')
    ->condition('tenant_id', $node->nid)
    ->execute();
}

/**
 * @param $placeholder - the name of the placeholder
 * @return mixed
 */
function placeholder_pane_load($placeholder) {
  $tenant = db_select('placeholder_panes', 'pp')
    ->fields('pp')
    ->condition('placeholder', $placeholder)
    ->execute()
    ->fetchObject();

  if (!empty($tenant->placeholder_settings)) {
    $tenant->placeholder_settings = unserialize($tenant->placeholder_settings);
    if (empty($tenant->placeholder_settings['allowed_bundles']) || !is_array($tenant->placeholder_settings['allowed_bundles'])) {
      $tenant->placeholder_settings['allowed_bundles'] = array();
    }
  }
  return $tenant;
}

/**
 * Make sure that if you return a tenant object
 * $tenant->title should contain the title
 * $tenant->placeholder should contain the placeholder object
 *
 * @param $placeholder
 * @return bool|mixed
 */
function placeholder_pane_tenant_load($placeholder, $langcode = NULL) {
  if (is_string($placeholder)) {
    $placeholder = placeholder_pane_load($placeholder);
  }
  if (!empty($placeholder->tenant_id) && !empty($placeholder->tenant_entity_type)) {
    if ($placeholder->tenant_entity_type === 'node') {
      if ($langcode && module_exists('translation')) {
        $translations = translation_node_get_translations($placeholder->tenant_id);
        if (!empty($translations[$langcode]->nid)) {
          $tnid = $translations[$langcode]->nid;
          if ($tnid != $placeholder->tenant_id) {
            $placeholder->tenant_source_id = $placeholder->tenant_id;
            $placeholder->tenant_id = $tnid;
          }

        }
      }
      $tenant = node_load($placeholder->tenant_id);
      $tenant->placeholder = $placeholder;
      return $tenant;
    }
  }
  return FALSE;
}

/**
 * Returns the render array representing the tenant
 *
 * @param $tenant
 * @param null $langcode
 * @return array
 */
function placeholder_pane_tenant_view($tenant, $langcode = NULL) {
  if (empty($tenant->placeholder)) {
    return array();
  }

  if ($tenant->placeholder->tenant_entity_type === 'node') {
    if (empty($tenant->placeholder->tenant_mode)) {
      $tenant->placeholder->tenant_mode = 'full';
    }
    return node_view($tenant, $tenant->placeholder->tenant_mode, $langcode);
  }
  return array();
}


/*
* Implements hook_ctools_plugin_directory().
*/
function placeholder_pane_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/'. $plugin_type;
  }
}

/**
 * Implementation of hook_form_node_form_alter().
 */
function placeholder_pane_form_node_form_alter(&$form, &$form_state) {

  if (!user_access('occupy placeholders')) {
    return;
  }

  $node = $form['#node'];

  // if translation node, do nothing
  // only source nodes can occupy placeholders
  if (!empty($node->nid) && !empty($node->tnid) && $node->nid != $node->tnid) {
    return;
  }

  // re-register placeholders if need be
  $registered = cache_get('placeholder_pane:complete');
  if (empty($registered->data)) {
    placeholder_pane_get_placeholders();
  }

  $placeholders = array();
  $tenants = array();

  $result = db_select('placeholder_panes', 'p')
    ->fields('p')
    ->condition('placeholder_settings', '%node:'. $node->type .'%', 'LIKE')
    ->execute();

  $p = 0;
  foreach ($result as $placeholder) {
    if (!empty($node->nid) && !empty($placeholder->tenant_id) && $node->nid == $placeholder->tenant_id) {
      $tenants[] = $placeholder;
    }
    else if (!empty($node->tnid) && !empty($placeholder->tenant_id) && $node->tnid == $placeholder->tenant_id) {
      $tenants[] = $placeholder;
    }
    else {
      $placeholders[$placeholder->placeholder] = $placeholder->placeholder_title;
    }
    $p++;
  }

  if (!$p || !user_access('occupy placeholders')) {
    return;
  }

  $form['placeholder_pane'] = array(
    '#type' => 'fieldset',
    '#title' => t('Placeholder panes'),
    '#tree' => TRUE,
    '#group' => 'additional_settings',
  );

  $form['placeholder_pane']['panes'] = array(
    '#type' => 'item',
    '#prefix' => '<div id="panes-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  $view_modes = placeholder_pane_get_view_modes($node->type);

  $t = 0;
  foreach ($tenants as $tenant) {
    $form['placeholder_pane']['panes'][$t]['placeholder'] = array(
      '#type' => 'select',
      '#options' => array('' => t('- none -')) + array($tenant->placeholder => $tenant->placeholder_title),
      '#title' => t('The node is placed in'),
      '#default_value' => empty($form_state['values']['placeholder_pane']['panes'][$t]['placeholder']) ? $tenant->placeholder : $form_state['values']['placeholder_pane']['panes'][$t]['placeholder'],
      '#group' => 'placeholder_pane',
    );

    $form['placeholder_pane']['panes'][$t]['mode'] = array(
      '#type' => 'select',
      '#options' => $view_modes,
      '#title' => t('View mode'),
      '#default_value' => empty($form_state['values']['placeholder_pane']['panes'][$t]['mode']) ? $tenant->tenant_mode : $form_state['values']['placeholder_pane']['panes'][$t]['mode'],
      '#group' => 'placeholder_pane',
    );
    $t++;
  }

  // additional placeholders (via Occupy another button)
  if (empty($form_state['a'])) {
    $form_state['a'] = ($p > $t) ? 1 : 0;
  }

  for ($i = 0; $i < $form_state['a']; $i++) {
    $form['placeholder_pane']['panes'][$t]['placeholder'] = array(
      '#type' => 'select',
      '#options' => array('' => t('- none -')) + $placeholders,
      '#title' => $t ? t('Also place this node in') : t('Place this node in'),
      '#default_value' => empty($form_state['values']['placeholder_pane']['panes'][$t]['placeholder']) ? '' : $form_state['values']['placeholder_pane']['panes'][$t]['placeholder'],
      '#group' => 'placeholder_pane',
    );

    $form['placeholder_pane']['panes'][$t]['mode'] = array(
      '#type' => 'select',
      '#options' => $view_modes,
      '#title' => t('View mode'),
      '#default_value' => empty($form_state['values']['placeholder_pane']['panes'][$t]['mode']) ? '' : $form_state['values']['placeholder_pane']['panes'][$t]['mode'],
      '#group' => 'placeholder_pane',
    );
    $t++;
  }

  // if there are free placeholders
  if ($p - $t) {
    $form['placeholder_pane']['panes']['add_more'] = array(
      '#type' => 'submit',
      '#value' => t('Occupy another placeholder'),
      '#submit' => array('placeholder_pane_add_tenant_form'),
      '#ajax' => array(
        'callback' => 'placeholder_pane_add_tenant_callback',
        'wrapper' => 'panes-fieldset-wrapper',
      ),
      '#prefix' => '<br>',
    );
  }

  $form['actions']['submit']['#submit'][] = 'placeholder_pane_node_form_submit';
}

function placeholder_pane_add_tenant_form($form, &$form_state) {
  $form_state['a']++;
  $form_state['rebuild'] = TRUE;
}

function placeholder_pane_add_tenant_callback($form, $form_state) {
  return $form['placeholder_pane']['panes'];
}

function placeholder_pane_node_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['nid']) && !empty($form_state['values']['placeholder_pane']['panes'])) {
    $nid = $form_state['values']['nid'];
    foreach ($form_state['values']['placeholder_pane']['panes'] as $key => $tenant) {
      $previous_placeholder = NULL;
      if (is_int($key)) {
        $previous_placeholder = $form['placeholder_pane']['panes'][$key]['placeholder']['#default_value'];
      }
      if (!empty($tenant['placeholder'])) {
        db_update('placeholder_panes')
          ->fields(array(
            'tenant_entity_type' => 'node',
            'tenant_id' => $nid,
            'tenant_mode' => empty($tenant['mode']) ? NULL : $tenant['mode'],
          ))
          ->condition('placeholder', $tenant['placeholder'])
          ->execute();
      }
      elseif (empty($tenant['placeholder']) && !empty($previous_placeholder)) {
        db_update('placeholder_panes')
          ->fields(array(
            'tenant_entity_type' => NULL,
            'tenant_id' => NULL,
            'tenant_mode' => NULL,
          ))
          ->condition('tenant_entity_type', 'node')
          ->condition('tenant_id', $nid)
          ->condition('placeholder', $previous_placeholder)
          ->execute();
      }
    }
  }
}

/**
 * Implements hook_panels_pane_insert().
 *
 * We need to make sure the placeholder pane is properly registered
 *
 * Fired before a new pane is inserted in the storage.
 *
 * @param stdClass $pane
 *   Pane that will be rendered.
 */
function placeholder_pane_panels_pane_insert($pane) {
  if ($pane->type === 'placeholder_pane' && !empty($pane->configuration['placeholder'])) {
    $placeholder = array(
      'placeholder' => $pane->configuration['placeholder'],
      'placeholder_title' => $pane->configuration['title'],
      'placeholder_settings' => array('allowed_bundles' => array_keys($pane->configuration['allowed_bundles'])),
      'module' => $pane->configuration['module'],
    );

    placeholder_pane_register($placeholder);
  }
}

/**
 * Implement hook_panels_pane_update().
 *
 * @param $pane
 */
function placeholder_pane_panels_pane_update($pane) {
  if ($pane->type === 'placeholder_pane') {
    placeholder_pane_panels_pane_insert($pane);
  }
}

/**
 * Fired before panes are deleted.
 *
 * @param array $pids
 *   Array with the panel id's to delete.
 */
function placeholder_pane_panels_pane_delete($pids) {
  foreach ($pids as $pid) {
    $pane = db_select('panels_pane', 'p')
      ->fields('p', array('type', 'subtype'))
      ->condition('pid', $pid)
      ->execute()
      ->fetchObject();

    if (empty($pane->type) || empty($pane->subtype) || $pane->type !== 'placeholder_pane') {
      return;
    }

    $content = placeholder_pane_placeholder_pane_content_type_content_type($pane->subtype);

    // delete only if we defined it
    if (empty($content['placeholder']['module']) || $content['placeholder']['module'] !== 'placeholder_pane') {
      return;
    }

    // delete only if only one pane uses it
    $count = db_select('panels_pane', 'p')
      ->fields('p', array('pid'))
      ->condition('type', $pane->type)
      ->condition('subtype', $pane->subtype)
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($count == 1) {
      db_delete('placeholder_panes')
        ->condition('placeholder', $pane->subtype)
        ->execute();
    }
  }
}

/**
 * Implements hook_views_api().
 */
function placeholder_pane_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'placeholder_pane') . '/views',
  );
}

